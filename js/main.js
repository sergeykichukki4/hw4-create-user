"use strict";

function checkCorrectness(message) {
    let txtString;

    do {
        txtString = prompt(message);
    } while (txtString === null || txtString === "")

    return txtString;
}

function createNewUser() {
    return {
        _firstName: checkCorrectness("Enter name"),
        _lastName: checkCorrectness("Enter last name"),

        set firstName(value) {
            this["_firstName"] = value;
        },

        get firstName() {
            return this._firstName;
        },

        set lastName(value) {
            this["_lastName"] = value;
        },

        get lastName() {
            return this._lastName;
        },

        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
    };
}

const user = createNewUser();
console.log(user.getLogin());

user.firstName = "Potter";
user.lastName = "Eva";

console.log(user);

Object.defineProperties(user, {
    '_firstName': {
        writable: false,
    },
    '_lastName': {
        writable: false,
    },
});

user.firstName = "Smirnov";
user.lastName = "Nikita";

console.log(user);







